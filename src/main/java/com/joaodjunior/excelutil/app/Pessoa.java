package com.joaodjunior.excelutil.app;

import java.util.Map;

public class Pessoa {
	
	String nome;
	String sobrenome;
	
	public Pessoa(Map<String, String> map) {
		super();
		this.nome = map.get("nome");
		this.sobrenome = map.get("sobrenome");
	}
	public Pessoa(String nome, String sobrenome) {
		super();
		this.nome = nome;
		this.sobrenome = sobrenome;
	}
	public Pessoa() {
		super();
	}


	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	
	

}
