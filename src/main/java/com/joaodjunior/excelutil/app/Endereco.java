package com.joaodjunior.excelutil.app;

import java.util.Map;

public class Endereco {
	
	String rua;
	int numero;
	
	public Endereco(Map<String, Object> map) {
		this.rua = (String) map.get("rua");
		this.numero = (Integer) map.get("numero");
	}
	public Endereco(String rua, int numero) {
		super();
		this.rua = rua;
		this.numero = numero;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	

}
