package com.joaodjunior.excel.util.app.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.joaodjunior.excelutil.app.Pessoa;

public class GenericTest {

	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, InstantiationException {
		
		Pessoa pessoa = new Pessoa("Joao", "Junior");
		
		String className = pessoa.getClass().getName();
        String[] methodsNames = {"getNome", "setNome"};

        Class<?> clazz = Class.forName(className);
        Object instanceOfClass =  pessoa.getClass().getConstructor(String.class, String.class).newInstance(pessoa.getNome(), pessoa.getSobrenome());

        for(String s : methodsNames) {
            Class<?>[] paramTypes = null;
            Method m = clazz.getDeclaredMethod(s, paramTypes);
            Object[] varargs = null;
            String g = (String) m.invoke(instanceOfClass, varargs);

            System.out.println(g);
        }

	}

}
